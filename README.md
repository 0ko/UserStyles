# My UserStyles

## Install

1. Install a style manager extension. The only usable choice right now is probably [Stylus](https://github.com/openstyles/stylus). It's free and open source, licensed under GNU GPL v3. Get it from [Chrome Web Store](https://chromewebstore.google.com/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne) | [Firefox add-ons](https://addons.mozilla.org/en-US/firefox/addon/styl-us/) | [GitHub Releases](https://github.com/openstyles/stylus/releases) | [More](https://github.com/openstyles/stylus#releases).
2. Install any style by following the **Install** link or the **Raw** button on the source files. When you have Stylus installed, it will handle `*.user.css` URLs and will bring up the installation/configuration page.

## Configure

These userstyles are configurable via the style manager. In some styles most of the changes enabled by default are reversible. Sometimes there are additional options that are disabled by default.  
To open the style configuration, go to the affected page, open the Stylus extension popup, locate the style and click the gear icon next to it.

## Discover

Consider browsing and installing styles from [my UserStyles.world profile](https://userstyles.world/~0ko) instead of Git repository. The Readme in this repo doesn't contain as much userstyles, and USw provides a better visual representation.

However, keep in mind that not all styles are published on UserStyles.world. So feel free to check out some of more niche entries in the Git repo if you feel interested.

It is not recommended to install userstyles directly from Codeberg, as file patches and therefore installation links sometimes change, breaking update checks in Stylus. Obviously, this does not apply to the styles that are exclusive to Git.

## Styles

Any feedback is welcome at [issues](https://codeberg.org/0ko/UserStyles/issues).


## Clean Telegram WebA/WebZ

<!-- no Preview -->

**[Install](https://userstyles.world/api/style/8012.user.styl)** | [UserStyles.world page](https://userstyles.world/style/8012) | [Install from Codeberg](https://codeberg.org/0ko/UserStyles/raw/main/Telegram-WebA/Clean/CleanTWA.user.css) | [Changelog](https://codeberg.org/0ko/UserStyles/commits/main/Telegram-WebA/Clean/CleanTWA.user.css)

Removes
- ads
- Stories
- Premium badges
- Premium promotions and UI elements that can only be used with Premium

There is little reason to install it if you have a Premium subscription.


## Clean Urban Dictionary

<a href="https://userstyles.world/preview/12363/0.webp" target="_blank">
	<img src="https://userstyles.world/preview/12363/0t.webp" alt="🖼Preview" title="🖼Click to open full image">
</a>

**[Install](https://userstyles.world/api/style/12363.user.styl)** | [UserStyles.world page](https://userstyles.world/style/12363) | [Install from Codeberg](https://codeberg.org/0ko/UserStyles/raw/main/Urban-Dictionary/Clean.user.css) | [Changelog](https://codeberg.org/0ko/UserStyles/commits/main/Urban-Dictionary/Clean.user.css)

Inspired by [Simple Urban Dictionary](https://userstyles.world/style/6345) but hides even more elements and contains other tweaks to simplify the UI.


## Clean YouTube

<a href="https://userstyles.world/preview/10175/5.webp" target="_blank">
	<img src="https://userstyles.world/preview/10175/5t.webp" alt="🖼Preview" title="🖼Click to open full image">
</a>

**[Install](https://userstyles.world/api/style/10175.user.styl)** | [UserStyles.world page](https://userstyles.world/style/10175) | [Install from Codeberg](https://codeberg.org/0ko/UserStyles/raw/main/YouTube/Clean/CleanYouTube.user.css) | [Changelog](https://codeberg.org/0ko/UserStyles/commits/main/YouTube/Clean/CleanYouTube.user.css)

Removes unnecessary things such as:
- shorts
- paid comments background
- channel logo/watermark in the right bottom corner of the video
- button to play next video
- newness dots which are indicators of new videos on the channel that you may have already watched
- other junk from sidebar
- voice search button

Formerly known as Enhancements for YouTube.


## Immersive Google Maps Street View

<a href="https://userstyles.world/preview/5386/1.webp" target="_blank">
	<img src="https://userstyles.world/preview/5386/1t.webp" alt="🖼Preview" title="🖼Click to open full image">
</a>

**[Install](https://userstyles.world/api/style/5386.user.styl)** | [UserStyles.world page](https://userstyles.world/style/5386) | [Install from Codeberg](https://codeberg.org/0ko/UserStyles/raw/main/Google-Maps/Immersive/Immersive.user.css) | [Changelog](https://codeberg.org/0ko/UserStyles/commits/main/Google-Maps/Immersive/Immersive.user.css)

Hides the entire UI or individual elements.


## Immersive Yandex Maps Panorama

<a href="https://userstyles.world/preview/11045/1.webp" target="_blank">
	<img src="https://userstyles.world/preview/11045/1t.webp" alt="🖼Preview" title="🖼Click to open full image">
</a>

**[Install](https://userstyles.world/api/style/11045.user.styl)** | [UserStyles.world page](https://userstyles.world/style/11045) | [Install from Codeberg](https://codeberg.org/0ko/UserStyles/raw/main/Yandex-Maps/Immersive-Panorama.user.css) | [Changelog](https://codeberg.org/0ko/UserStyles/commits/main/Yandex-Maps/Immersive-Panorama.user.css)

Same as Immersive Street View but for Yandex Maps.


## Clean VK

<a href="https://userstyles.world/preview/4733/17.webp" target="_blank">
	<img src="https://userstyles.world/preview/4733/17t.webp" alt="🖼Preview" title="🖼Click to open full image">
</a>

**[Install](https://userstyles.world/api/style/4733.user.styl)** | [UserStyles.world page](https://userstyles.world/style/4733) | [Install from Codeberg](https://codeberg.org/0ko/UserStyles/raw/main/VK/Clean/CleanVK.user.css) | [Changelog](https://codeberg.org/0ko/UserStyles/commits/main/VK/Clean/CleanVK.user.css)

Almost ultimate debloater for the VK website.

Formerly known as Enhancements for VK.


## VK no Avatars

<!-- no Preview -->

**[Install](https://userstyles.world/api/style/10473.user.styl)** | [UserStyles.world page](https://userstyles.world/style/10473) | [Install from Codeberg](https://codeberg.org/0ko/UserStyles/raw/main/VK/NoAvatars/VKnoAvatars.user.css) | [Changelog](https://codeberg.org/0ko/UserStyles/commits/main/VK/NoAvatars/VKnoAvatars.user.css)

Removes avatars on VK to make it feel more outdated.


## Fast ConVars

<a href="https://userstyles.world/preview/10448/4.webp" target="_blank">
	<img src="https://userstyles.world/preview/10448/4t.webp" alt="🖼Preview" title="🖼Click to open full image">
</a>

**[Install](https://userstyles.world/api/style/10448.user.styl)** | [UserStyles.world page](https://userstyles.world/style/10448) | [Install from Codeberg](https://codeberg.org/0ko/UserStyles/raw/main/ConVars/Fast/Fast.user.css) | [Changelog](https://codeberg.org/0ko/UserStyles/commits/main/ConVars/Fast/Fast.user.css)

Removes
- ads
- loading screen so you can use the site without waiting for all the 4k images from Reddit to load
- changelogs (some of them are huge)
